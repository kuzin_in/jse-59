package ru.kuzin.tm.service;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.kuzin.tm.api.service.ITokenService;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Service
public final class TokenService implements ITokenService {

    @Nullable
    private String token;

}