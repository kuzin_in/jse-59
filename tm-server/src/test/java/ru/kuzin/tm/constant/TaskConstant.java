package ru.kuzin.tm.constant;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kuzin.tm.dto.model.TaskDTO;
import ru.kuzin.tm.enumerated.Sort;
import ru.kuzin.tm.enumerated.Status;
import ru.kuzin.tm.model.Task;

public interface TaskConstant {

    int INIT_COUNT_TASKS = 5;

    @Nullable
    TaskDTO NULLABLE_TASK = null;

    @Nullable
    Task NULLABLE_TASK_MODEL = null;

    @Nullable
    String NULLABLE_USER_ID = null;

    @Nullable
    String EMPTY_USER_ID = "";

    @Nullable
    String NULLABLE_TASK_ID = null;

    @NotNull
    String EMPTY_TASK_ID = "";

    @NotNull
    Sort CREATED_SORT = Sort.BY_CREATED;

    @NotNull
    Sort NAME_SORT = Sort.BY_NAME;

    @NotNull
    Sort STATUS_SORT = Sort.BY_STATUS;

    @Nullable
    Sort NULLABLE_SORT = null;

    @Nullable
    Integer NULLABLE_INDEX = null;

    @NotNull
    Status IN_PROGRESS_STATUS = Status.IN_PROGRESS;

}